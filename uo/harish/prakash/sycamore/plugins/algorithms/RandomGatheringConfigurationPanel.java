/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JCheckBox;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;

/**
 * @author harry
 *
 */
public class RandomGatheringConfigurationPanel extends SycamorePanel {

	private JCheckBox checkboxQuitOnTwo;

	private static RandomGatheringConfigurationPanel uniqueInstance;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	@Override
	public void setAppEngine(SycamoreEngine appEngine) {}

	@Override
	public void updateGui() {}

	@Override
	public void reset() {}

	private RandomGatheringConfigurationPanel() {
		setupGUI();
	}

	private void setupGUI() {

		GridBagLayout layout = new GridBagLayout();
		layout.columnWeights = new double[] { 1 };
		setLayout(layout);

		GridBagConstraints c;

		// Check boxes
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		add(getCheckboxQuitOnTwo(), c);
		// End of Check boxes
	}

	/**
	 * @return the checkboxQuitOnTwo
	 */
	private JCheckBox getCheckboxQuitOnTwo() {

		if (checkboxQuitOnTwo == null) {
			checkboxQuitOnTwo = new JCheckBox("Terminate when two robots are looking only at each other");
			checkboxQuitOnTwo.setSelected(true);
		}
		return checkboxQuitOnTwo;
	}

	public boolean shouldQuitOnTwo() {

		return getCheckboxQuitOnTwo().isSelected();
	}

	/**
	 * @return the uniqueInstance
	 */
	public static RandomGatheringConfigurationPanel getUniqueInstance() {

		if (uniqueInstance == null) {
			uniqueInstance = new RandomGatheringConfigurationPanel();
		}
		return uniqueInstance;
	}
}
