/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.engine.SycamoreObservedRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;
import it.diunipi.volpi.sycamore.plugins.algorithms.AlgorithmImpl;
import java.util.Vector;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 *
 * @author harry
 */
@PluginImplementation
public class RandomGathering extends AlgorithmImpl<Point2D> {

	private final String		_author					= "Harish Prakash";
	private final String		_pluginShortDescription	= "Random Gathering";
	private final String		_pluginLongDescription	= "Robots gathering at random locations";
	private final String		_references				= null;
	private final String		_pluginName				= "RandomGathering";
	private java.util.Random	rand;
	private Long				_terminateAt;

	@Override
	public void init(SycamoreObservedRobot<Point2D> robot) {

		rand = new java.util.Random();
	}

	@Override
	public Point2D compute(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		if (observations == null || observations.size() < 1) {
			setFinished(true);
			return null;
		}

		else if (observations.size() == 1 && RandomGatheringConfigurationPanel.getUniqueInstance().shouldQuitOnTwo() && rand.nextBoolean()) {
			// setFinished(true);
			return null;
		}

		else if (_terminateAt == null) {
			_terminateAt = System.currentTimeMillis() + (long) (((double) 15000) / caller.getSpeed());
		}

		else if (System.currentTimeMillis() > _terminateAt) {
			setFinished(true);
			_terminateAt = null;
			return null;
		}

		Point2D common = caller.getLocalPosition();
		for (Observation<Point2D> robot : observations) {
			if (robot.getRobotPosition().distanceTo(common) > SycamoreSystem.getEpsilon())
				return observations.get(rand.nextInt(observations.size())).getRobotPosition();
		}

		setFinished(true);
		return null;
	}

	@Override
	public String getReferences() {

		return _references;
	}

	@Override
	public SycamoreEngine.TYPE getType() {

		return SycamoreEngine.TYPE.TYPE_2D;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _pluginShortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _pluginLongDescription;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return RandomGatheringConfigurationPanel.getUniqueInstance();
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

}
